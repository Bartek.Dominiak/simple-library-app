package library;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class LibraryApp {


    private Library library = new Library();
    private BorrowSystem borrowSystem = new BorrowSystem();

    public Library getLibrary() {
        return library;
    }

    public void System() throws IOException {
        boolean flag = true;

        while (flag) {
            printMenu();
            String choice = reader();
            choice = choice.toLowerCase();
            switch (choice) {
                case "1":
                    break;
                case "2":
                    printBooks();
                    break;
                case "3":
                    searchBook();
                    break;
                case "4":
                    borrowBook();
                    break;
                case "5":
                    addBook();
                    break;
                case "6":
                    removeBook();
                    break;
                case "exit":
                    System.out.println("Wychodzisz z biblioteki");
                    flag = false;
                    break;
                default:
                    System.out.println("Nie prawidłowa wartość, wpisz ponownie");

            }

        }


    }


    private void removeBook() throws IOException {
        System.out.println("Podaj nr ISBN książki, którą chcesz usunąć:");
        String ISBN = reader();

        try {
            if (library.isBookExist(ISBN)) {

                library.removeBook(ISBN);
                System.out.println("Książka została usunięta");
            } else {
                System.out.println("Taka książka nie istnieje");
            }
        } catch (NumberFormatException e) {
            System.out.println("To nie nr ISBN");
        }
    }


    private void borrowBook() throws IOException {
        System.out.println("Podaj numer ISBN książki, którą chcesz wypożyczyć:");
        String ISBN = reader();
        if (library.isBookExist(ISBN)) {
            if (borrowSystem.isBorrowedBookEmpty()) {
                System.out.println("Nie ma żadnej książki wypożyczonej!");
            }
            if (borrowSystem.borrowBook(ISBN)) {
                System.out.println("Książka została wypożyczona na 30 dni do " + borrowSystem.getBorrowedBooks().get(ISBN));
            } else {
                System.out.println("Ta książka jest już wypożyczona do " + borrowSystem.getBorrowedBooks().get(ISBN));
            }
        } else {
            System.out.println("Nie ma takiej książki");
        }


    }

    private void searchBook() throws IOException {
        System.out.println("Wpisz frazę lub liczbę po której chcesz znaleźć książkę:");
        String parameter = reader();
        List<Book> bookList = library.isBookWithParametrExist(parameter);
        if (bookList.isEmpty()) {
            System.out.println("Nie ma takiej książki w systemie!");
        } else {
            for (Book book : bookList) {
                System.out.println(book);
            }
        }

    }


    private void printMenu() {
        System.out.println("1 - Wydrukuj menu");
        System.out.println("2 - Wydrukuj listę książek");
        System.out.println("3 - Znajdź książkę");
        System.out.println("4 - Wypożycz książkę");
        System.out.println("5 - Dodaj książkę do systemu");
        System.out.println("6 - Usuń książkę z systemu");
        System.out.println("Jeśli chcesz wyjść wpisz 'Exit'");
    }

    private void addBook() throws IOException {
        System.out.println("Podaj nr ISBN książki, którą chcesz dodać:");
        String ISBN = reader();
        try {


            if (!library.getBookMap().containsKey(ISBN)) {
                System.out.println("Podaj tytuł książki:");
                String title = reader();
                System.out.println("Podaj autora książki:");
                String author = reader();
                System.out.println("Podaj rok wydania:");
                String year = reader();
                int yearPub = Integer.parseInt(year);
                Book newBook = new Book(title, author, yearPub, ISBN);
                library.getBookMap().put(ISBN, newBook);
                System.out.println("Książka " + library.getBookMap().get(ISBN) + " została dodana do systemu");
            } else {
                System.out.println("Książka już jest w systemie!");
            }
        } catch (NumberFormatException e) {
            System.out.println("To nie liczba!");
            printMenu();
        }

    }

    private void printBooks() {
        if (library.getBookMap().isEmpty()) {
            System.out.println("Biblioteka nie posiada książek");
        } else {
            library.printBooks();
        }
    }

    private static String reader() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return reader.readLine();
    }


}
