package library;

import java.util.Objects;
import java.util.StringJoiner;

public class Book {

    private String title;
    private String author;
    private int yearOfPublication;
    private String ISBN;

    public Book(String title, String author, int yearOfPublication, String ISBN) {
        this.title = title;
        this.author = author;
        this.yearOfPublication = yearOfPublication;
        this.ISBN = ISBN;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public int getYearOfPublication() {
        return yearOfPublication;
    }

    public String getISBN() {
        return ISBN;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Book.class.getSimpleName() + "[", "]")
                .add("ISBN: " + ISBN)
                .add("Tytuł: " + title)
                .add("Autor: " + author)
                .add("Rok wydania: " + yearOfPublication)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return yearOfPublication == book.yearOfPublication &&
                ISBN == book.ISBN &&
                Objects.equals(title, book.title) &&
                Objects.equals(author, book.author);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, author, yearOfPublication, ISBN);
    }
}
