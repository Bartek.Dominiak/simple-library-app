package library;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class BorrowSystem {

    private Map<String, String> borrowedBooks;


    public BorrowSystem() {
        this.borrowedBooks = new HashMap<>();
    }


    public Map<String, String> getBorrowedBooks() {
        return borrowedBooks;
    }


    private String giveLastDateOfReturning() {
        LocalDateTime currentDate = LocalDateTime.now();
        LocalDateTime timeOfReturn = currentDate.plusDays(30);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        return timeOfReturn.format(formatter);

    }

    public boolean borrowBook(String ISBN) {
        if (!isBookBorrowed(ISBN)) {
            borrowedBooks.put(ISBN, giveLastDateOfReturning());
            return true;
        }
        return false;
    }

    public boolean isBookBorrowed(String ISBN) {
        if (!isBorrowedBookEmpty()) {
            return borrowedBooks.containsKey(ISBN);
        }
        return false;
    }

    public boolean isBorrowedBookEmpty() {
        return borrowedBooks.isEmpty();
    }


}
