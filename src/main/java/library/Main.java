package library;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        LibraryApp libraryApp = new LibraryApp();



        Book book1 = new Book("Władca Pierścieni", "Tolkien", 2004, "1234566");
        Book book2 = new Book("Władca Pierścieni", "Tolkien", 2004, "12345546");
        Book book3 = new Book("Łowcy", "Tolkien", 2004, "1236786");


        libraryApp.getLibrary().addBook(book1);
        libraryApp.getLibrary().addBook(book2);
        libraryApp.getLibrary().addBook(book3);


        libraryApp.System() ;


    }
}
