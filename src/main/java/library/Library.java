package library;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Library {

    private Map<String, Book> bookMap;

    public Library() {
        this.bookMap = new HashMap<>();
    }

    public Map<String, Book> getBookMap() {
        return bookMap;
    }

    public void printBooks() {

            for (Book book : bookMap.values()) {
                System.out.println(book);
            }

    }


    public List<Book> isBookWithParametrExist(String parametr) {
        List<Book> bookList = new ArrayList();
        for (Book book : bookMap.values()) {
            if (book.getAuthor().contains(parametr) ||
                    book.getTitle().contains(parametr) ||
                    book.getISBN().contains(parametr) ||
                    Integer.toString(book.getYearOfPublication()).contains(parametr)) {
                bookList.add(book);
            }

        }
        return bookList;
    }



    public boolean isBookExist(String ISBN) {
        return bookMap.containsKey(ISBN);
    }

    public void addBook(Book book) {
        String key = book.getISBN();
        bookMap.put(key, book);
    }

    public void removeBook(String ISBN) {
        if (isBookExist(ISBN)) {
            bookMap.remove(ISBN);
        }

    }


}
